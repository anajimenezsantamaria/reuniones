# Resumen reunión (10, Octubre, 2019)
# Asistentes

- Jorge Barrachina (@ntkog) (Esri España)
- J. Manrique Lopez (@jsmanrique)
- David Gómez G. (@dgomezg) (Liferay)

# Notas

[Notas de reunión anterior](https://gitlab.com/devrel-aguacates/reuniones/blob/master/2019-09-26-notas-reunion-avocados-coffee.md)

El tema principal para hoy es:
**¿Qué es una comunidad?**

**Definiciones de los asistentes**

**Manrique**: 

- La experencia de dónde vienes cuentan a la hora de definir qué es comunidad. 
- CLSX Summit, Unconference. El ejemplo de Cherokee. Referencia al evento de Granada de Open Source.
- Idea de compartir, y crecer conjuntamente proyecto y personas ( Peligros : otras motivaciones e intereses en el mundo de las comunidades)
- Personas con un interés común, pero también con intereses particulares
- Las personas que *lideran* hacen que sus intereses lideren 

**David Gómez**:

- Se basa en tener varios intereses comunes. Hay segmentaciones muy de grano fino . Los meetups no son comunidades
- Informe Community RoundTable 2018, lo define muy bien. Entramos en una comunidad como "consumidores" . Luego supuestamente tú también deberías contribuir, o tienes cosas que aportar.
- Instrumentalizaciones de las comunidades pueden "distorsionar" el concepto de comunidad 

**Jorge Barrachina**:

- El concepto de comunidad no debe ser algo dogmático. 
- Muchas veces parece que importa más el volumen(cuanta gente viene) que el contenido (qué se comparte).
- En cuanto a los códigos de conducta: ¿Tenemos legitimidad "moral" para decirle a la gente cómo debe comportarse?. Si objetivo de la comunidad (en las comunidades técnicas) es tratar o debatir sobre temas técnicos, ¿donde está el límite de imposición de reglas? . 
- Cuando vamos al teatro, a un concierto, a ver un partido...¿Firmas un código de conducta? Si no es así, ¿Por qué se perciben tan distintos los eventos mencionados a los eventos técnicos? 
- La diversidad se queda en diversidad racial, de genero, pero ¿qué pasa con la diversidad de ideas?

El CoC (Código de Conducta) se ha intentando elevar a categoría de Ley para solucionar todos los problemas de la comunidad

Necesitas transmitir unas ciertas normas de "etiqueta". Cuando no había algo formal, se empezó a trabajar en ciertas reglas, que han acabado en una especie de Ley/herramienta/justificación para ciertas decisiones de acciones a tomar.

La gente que organiza una comunidad decide *lo que es la comunidad*. Y las comunidades 
no deberían tener una persona "organizadora".

Los CoC, educar frente a adoctrinar. Cuando se va a un concierto, 
¿necesitas firmar un CoC? Incluso habiendo ciertas normas

Las comunidades se basan en confianza, ¿por qué declarar ciertas trabas?

Puedes establecer ciertas reglas, para educar, y dar conciencia de que ciertas 
acciones tienen consecuencias? Visibilizar la "etiqueta" del grupo.

Concepto de Net-etiquette . ¿Se ha perdido? Eran cosas más de operativa antes. 
Conforme hemos sido más capaces de reunirnos físicamente están surgiendo más 
problemas. La comunidad debería ser autónoma para poder resolver esos problemas 
de forma natural.

La comunidad no debería tener *Manager*, sino personas facilitadoras. La 
organización asumen un montón de tareas como parte de la evolución de las
comunidades.

"Yo puedo hablar con Europa, ¿quién es Europa?". 

Hay que fomentar que haya un mayor número de organizadores. 

Las comunidades que llevan tiempo y han conseguido sobrevivir tienen un 
"modelo de gobernanza". Ignorar en vez de expulsar (dependerá de la comunidad).

**El componente generacional** y cómo afecta a la distintas percepción o idea de 
qué es una comunidad. Comunidades formadas por gente que va desde 18 años hasta 
los 40 y más allá, con distintas experiencias e ideas de cómo han vivido las comunidades.

**Problemas recurrentes.**

- No show-up
- Presión sufrida por parte de la organización de generación de contenidos/reuniones con frecuencia.
- Los estándares normativos de cómo hay que organizar una comunidad/evento/meetup: (hay que tener cervezas, hay que tener reunión mensual, hay que tener bolsa con flyers a la entrada, ....) 

Tip de Pintxito: Venue con la mitad de capacidad. 

No me siento capacitado para decirle a la gente cómo tiene que hacer las cosas.
Pero sí de cómo me gustaría que sucedieran las cosas, y que la comunidad decida 
si es así o no.

Ser consciente de que la comnuidad al final es un colectivo de personas, pero que la responsabilidad individual afecta directamente en cómo se producen esas interacciones. Hay una diferencia entre consensuar unas "reglas del juego" vs imponerlas.

# Take-aways

[Community RoundTable 2018 (ver la explicación de la evolución de las comunidades)](https://communityroundtable.com/what-we-do/research/the-state-of-community-management/state-community-management-2018/)

[The power of diverse thinking - Matthew Syed](https://www.youtube.com/watch?time_continue=2&v=JRvOaIB2nTE)

# Acciones

IDEA: intentar incrementar el número de organizadores de las comunidades en las que tenemos responsabilidad como primer paso empujar la comunidad hacia ese estado de "Auto-organización"

IDEA: Fomentar otras actividades alrededor de la comunidad que no sean solo charlas: potenciar comunicación, conversaciones online, compartir información y recursos... Sacar la comnidad del corsé de que solo son meetups que se hacen regularmente. Fomentar la parte activa de la comunidad. 

Fomentar la parte asíncrona de las comunidades, que es la que fomenta los intereses comunes, la parte activa. 

¿qué se puede hacer más allá de Meetups? ¿porqué se hacen sólo meetups y no otras cosas?

# Próxima reunión
24 de Octubre, 11am.

**Tema**: Ideas para el fomento de la parte asíncrona de las comunidades