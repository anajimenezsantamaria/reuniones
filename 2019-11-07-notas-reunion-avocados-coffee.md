# Resumen reunión (7, Noviembre, 2019)
# Asistentes

* @benjagra
* @dgomezg
* @jsmanrique

# Notas

Topic: **¿DevRel en el departamento de Marketing?**

¿Cuál es el objetivo de DevRel?

- Hacer de puente entre muchos departamentos: producto, marketing, ventas, etc.
- Parece que las únicas opciones: cuelgas de marketing o de ingeniería
    - Colgar de ingeniería te impide establecer puentes con otros departamentos
    - Marketing se mide por ventas, y DevRel siendo una forma de marketing, es distinta (con otro timing)
- En Liferay, se reporta directamente al CTO (desde hace un año aproxiamadamente, antes se dependía de ingeniería)
- Se gana independecia
- La comunicación con ingeniería cuesta algo más, al ser un departamento externo, y por tanto cuesta "influir" en ingeniería. Sin embargo, la capacidad que debería tener DevRel de influir en cómo se construye el producto no debería perderse. Debería poder seguir sirviendo para DevRel pueda canalizar la comunicación desde fuera hacia ingeniría.

Interesante el caso de Community Manager en Magento, [entrevista en el Community Pulse](http://communitypulse.io/40-oss-cmgr/), 
por similitudes y diferencias con Liferay. DevRel como relación con el ecosistema de desarrollo, mientras que el community 
manager open source tiene que tener en cuenta las personas usuarias.

Intentar encajar DevRel en otro departamento, hace que las métricas de media de DevRel acaban siendo condicionadas por las del departamento. 
Por Ejemplo: si depende de Marketing, se acaba tratando de medir números inmediatos de cada acción: asistentes a eventos, asistentes que piden un trial, 
asistenes que se registran en la plataforma, ... Marketing suele ir conectado con ventas y generación de oportunidades.

Los objetivos de DevRel no debe ser sólo el de responder a las cuestiones enviadas de la comunidad. 
Debe ser responsable de fomentar la comunición entre ingeniería y el mundo externo.

Interesante iniciativa en Liferay: DevRel participando en los dailys de ingeniería para fomentar la relación.

Otro enfoque. DevRel para la "comunidad desarrollo interna de una organización". Dependencia del equipo de Innovación: El papel de Developer Relations 
para ayudar a mejorar la forma de trabajo en los equipos de forma interna. Unificar la forma de trabajo e iniciativas duplicadas que están repartidas 
en corporaciones muy grandes. También está alineado con la tarea de proponer nuevas herramientas y procesos para trabajar (y pasar por los procesos de certificación)

Para los cambios de procesos internos en grandes corporaciones, merece la pena evaluar [InnerSource](http://innersourcecommons.org/). ¿puede ser DeveloperRelations un facilitador para introducirlo?

# Take-aways

Hay una charla de Hadi Hariri en la Tarugoconf de 2017 en la que explica cómo organizaron el departamento en Jetbrains.

Community Pulse podcast: http://communitypulse.io/

[InnerSource Commons](http://innersourcecommons.org/)
- Learning path: http://innersourcecommons.org/resources/learningpath/
- Patterns: https://github.com/InnerSourceCommons/InnerSourcePatterns
- Books: http://innersourcecommons.org/resources/books/
- ISC Spring Summit 2020 (¡en Madrid!): http://innersourcecommons.org/events/isc-spring-2020/

[InnerSource Spain](https://www.meetup.com/es/innersource-spain/)

Zalando Tech Radar: https://opensource.zalando.com/tech-radar/

Libro de [Mary Thengvall](https://www.marythengvall.com/) (lectura obligada): https://www.amazon.es/Business-Value-Developer-Relations-Communities

Nuevo libro de [Jono Bacon](https://www.jonobacon.com/): https://www.jonobacon.com/books/peoplepowered/

# Acciones

Para siguiente café: ¿Cómo se mide KPIs y DevRel en los distintos departamentos?
- Post en el blog de Bitergia sobre el tema: https://blog.bitergia.com/2019/05/28/kpis-and-metrics-for-devrel-programs/