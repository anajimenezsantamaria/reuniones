# Resumen reunión :es: :avocado: :coffee: (19, Diciembre, 2019)
# Asistentes

* @anajsana
* Francisco Picolini
* @ntkog
* @jsmanrique

# Notas

Bromas iniciales sobre el AWS re:Invent y SWAG

## DevRel Con London

Más grande que el DevRelCon de Tokio

Topic más repetido: ¿Cuál es valor de DevRel?

Modelos de DevRel

- Nueva fórmula : Orbit [Communities aren't funnels](https://noti.st/dzello/wBONlC/communities-arent-funnels-try-the-orbit-model-instead) : ¿ Está maduro ? Parece que están en los inicios 

Los fundadores vienen de Keen.io

Hay 4 actores/roles en la comunidad:
- Ambassadors
- Fans
- Users
- Observers

DevRel (De qué departamento depende) -> Muchas empresas se basan en captar leads

De quién se depende no excluye el problema de fondo: ¿cuál es el valor que aporta?

Lo importante será cual es el objetivo de la Empresa, y cómo se adapta el rol de DevRel. 

Inmediatez de resultados: qué espera la empresa (Gestión de expectativas desalineadas entre equipos de Devrel y las empresas)

Un DevRel puede aportar en muchas áreas de la empresa. La clave está en ¿qué valor me aportas? . "la sostenibilidad"
de la empresa depende de la comunidad que tienes alrededor -> ¿ Es esto cierto, qué matices hay?

Comunidad es más que cliente: promociona, mejora, comparte, etc. producto

Si la compañía no tiene claro (o no comunica) para qué quiere una comunidad, la "evaluación" de la actividad de DevRel va a ser problemática

¿CRM para DevRels? ¿Están los objetivos claros en cuanto a qué esperas de una comunidad?

Como curiosidad sobre CRM's y protección de datos, echadle un vistazo a este artículo: [tarjetas de visita y GDPR](https://www.derecho.com/articulos/2018/10/17/tarjetas-de-visita-y-rgpd/)

Uno de los beneficios de formar comunidades de DevRel es generar conocimiento de cómo hacer cosas, compartir estrategias, herramientas y poder luego defender mejor el valor de nuestras acciones de cara a la empresa.

¿Tenemos algún ejemplo paradigmático de DevRel "bien hecho"?
- **Google (GDG's)** - Awareness . Fuerte inversión durante muchos años.
- Github & Gitlab  

# Take-aways

* [The secret life of open Source Developers](https://www.youtube.com/watch?v=Q187JGeXueA)
* [DevRel Qualified Leads: Repurposing A Common Business Metric To Prove Value](https://www.marythengvall.com/blog/2019/12/14/devrel-qualified-leads-repurposing-a-common-business-metrics-to-prove-value)


# Acciones
