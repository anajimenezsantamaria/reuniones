# Resumen reunión :es: :avocado: :coffee: (27, Febrero, 2020)

# Asistentes

- Javier Ramirez
- Maria Encimar
- Jorge Barrachina (@nktog)
- Manrique Lopez (@jsmanrique)

# Notas

## Topic 1: El Unconference que no fue

Aprender: no nos organizamos bien. ¿Cómo lo recuperamos?

Poco compromiso, pero seguro que había motivos más que suficientes seguramente para no ir. 

Necesitamos darle un toque de realidad.

Quizá necesitemos masa crítica.

Mejorar la comunicación interna, que seamos conscientes de las dificultades de tiempo de toda la gente.

Quizás mejor la unconference en horario laboral. Problemas de conciliación , los fines de semana son difíciles de encajar.

Reflexión y empezar en pequeñito. Si el tiempo es breve, que sea de calidad. Mejor pequeño con gente realmente interesada en aportar que de abrir a hacer un gran encuentro

Gestión de expectativas

Conclusión: Búsqueda de fechas, y para adelante!

## Topic 2: El debate del mundo open source

Ha sido tan intenso que ni hemos tomado notas (para confirmar la teoría de María que no podemos hacer 2 cosas a la vez!)

Enlace aportado por Jorge: https://www.notion.so/El-valor-del-Software-Find-the-sweet-spot-31055469f7584390bcd5267c2afcc6d6

Keynote de Adam Jacobs en OSCON: https://www.youtube.com/watch?v=8q5o-4pnxDQ