# Reuniones de DevRel Aguacates

La comunidad de [devrel.es](https://devrel.es), también conocida como *DevRel 
Aguacates*, se reune 
[cada 2 jueves de forma "virtual"](https://arewemeetingyet.com/Madrid/2019-10-10/11:00/b/Avocados%20Coffee%20e-Meeting#eyJ1cmwiOiJodHRwczovL21lZXQuaml0LnNpL0F2b2NhZG9zQ29mZmVlIn0=) 
a través de una videollamada para tratar distintos temas relacionados con la 
actividad de *DevRel* de las personas que la componen.

Este repositorio recoge las notas de las reuniones que vaya teniendo el grupo.

# Formato de las notas

Las notas de las reuniones se guardan en este repositorio como un fichero
[markdown](https://es.wikipedia.org/wiki/Markdown) con el siguiente esquema:

```markdown
# Resumen reunión (día, mes, año)
# Asistentes

(listado de asistentes a la reunión)

# Notas

(notas cogidas durante la reunión)

# Take-aways

(vídeos, blog posts, ó documentos comentados durante la reunión para ver)

# Acciones

(listado de acciones a ejecutar una vez acabada la reunión, indicando persona
responsable y acción concreta a realizar)

```

El fichero se guarda con un nombre del tipo:
`(año)-(mes)-(día)-notas-reunion-(nombre de reunion).md`

# Recogida de notas durante las reuniones

Para generar el fichero, se toman las notas de forma colaborativa usando 
[hackmd.io](https://hackmd.io) y una plantilla como la indicada anteriormente.

# Contribuir

Las notas se toman de forma colaborativa durante las reuniones. Aún así, puede 
que algún punto se quede olvidado o poco claro.

Si queréis, podéis [abrir un *issue*](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#create-a-new-issue)
para preguntar cualquie duda, y así queda reflejada en nuestro 
[listado de *issues*](https://gitlab.com/devrel-aguacates/reuniones/issues).

Cualquiera puede hacer un [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) 
de este repositorio para solicitar un [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/)
con los cambios que estime necesarios.

Y por supuesto, cualquiera puede asistir a las 
[reuniones](https://arewemeetingyet.com/Madrid/2019-10-10/11:00/b/Avocados%20Coffee%20e-Meeting#eyJ1cmwiOiJodHRwczovL21lZXQuaml0LnNpL0F2b2NhZG9zQ29mZmVlIn0=)
para colaborar y contribuir.

# Licencia

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional</a>.