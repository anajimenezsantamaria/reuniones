# Resumen reunión (21, 11, 2019)
# Asistentes

* Álvaro Navarro (@alnacle) (Amadeus)
* David Gómez G. (@dgomezg) (Liferay)
* Jorge Barrachina (@ntkog) (Esri España)
* J. Manrique Lopez (@jsmanrique) (Bitergia)

# Notas

TOPIC: KPI de DevRel

Jorge : Medir la progresión de una desarrollador en tu tecnología.
Las plataformas de e-learning ya tienen algo así
Pero, cómo cualifico yo a la gente de mi comunidad?

David : Programas de Champions, GDG's , Java, "Heroes" 

Manrique : Programa de Heroes de AWS. Gitlab han abierto una call para que las personas interesadas "demuestren" sus hitos con la tecnología. En la lista no había mujeres. En el caso de Gitlab aplicas, en el AWS te nombran


Álvaro : En Amadeus están diseñando un funnel para identificar patrones de la evolución de los desarrolladores,para ver que insights puede darles el funnel para sacar accionables.


David : ¿Gamificación? 

Álvaro : Cómo justifico a la dirección el que estamos haciendo bien nuestro trabajo, desde el discovery al crecimiento, o identificar el momento en el que se nos cae algún desarrollador en el camino. 


Manrique : En el próximo DevRel de Londres : "Tu comunidad no es un funnel" (incluir enlace cuando publiquen la charla), Developer Journey (ídem de la anterior) . Hay mucho de marketing de lo que se puede aprender para aplicar a Devrel.

Caso de Homebrew : Enlace al artículo.

Diferencia de perspectivas desde open source y la parte enterprise , en cuanto a operativa .

En Bitergia están haciendo research en cuanto a profifency en distintos lenguajes .

Jorge : Referencia a un post de Neurok . Usar grafos para representar y visualizar las conexiones de la gente de la comunidad y ponderar las aristas de esas interacciones para caracterizar dichas interacciones. (Frecuencia de conexión entre pares, tipo de interacción(etiquetarla) ). A partir de ahí se pueden buscar patrones e interpretarlos.

Manrique : CNCF (Cloud Native Computing Foundation )

Álvaro : ¿OKR vs KPI ? Lo están planteando en Amadeus.

Manrique : Experiencia propia. Intentaron implementarlo , pero la experiencia no fue satisfactoria . Lo más dificil no es poner los objetivos , lo dificil es los accionables (y medirlo de manera que refleje el avance) y no perder perspectiva

Álvaro : A nivel de Devrel no tienen todavía definidos los OKR's . Hay cosas que son muy medibles , pero otras no. Ejemplo : mejorar el onboarding del usuario. 

Jorge : .... A completar

Manrique : Goals questions metrics. A veces pierdo la visión del objetivo. Ej : Un Objetivo de ventas: la disonancia entre Objetivo y OKR que deriva de ese objetivo. Ciclo de Deming 


# Take-aways

https://blog.bitergia.com/2019/05/28/kpis-and-metrics-for-devrel-programs/

The Open Source Contributors Funnel
https://mikemcquaid.com/2018/08/14/the-open-source-contributor-funnel-why-people-dont-contribute-to-your-open-source-project/

The connected communities analysis
https://conferences.oreilly.com/oscon/oscon-or-2019/public/schedule/detail/75932
https://blog.bitergia.com/2019/05/03/cross-communities-collaboration-a-new-era-to-develop-communities/

Talks at DevRel Con London:
https://london-2019.devrel.net/speaker/igor-lukanin/
https://london-2019.devrel.net/speaker/josh-dzielak/

Goals - Questions - Metrics
https://en.wikipedia.org/wiki/GQM

Deming Circle
https://en.wikipedia.org/wiki/PDCA

Aprendizaje personalizado (A partir de medir distintas acciones de la gente de la comunidad)
https://neurok.es/aprendizaje-personalizado-learning/

# Acciones